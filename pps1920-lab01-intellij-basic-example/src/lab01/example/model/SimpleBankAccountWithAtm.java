package lab01.example.model;

/**
 * This class represent a particular instance of a BankAccount.
 * In particular, a Simple Bank Account allows always the deposit
 * while the withdraw is allowed only if the balance greater or equal the withdrawal amount
 */
public class SimpleBankAccountWithAtm extends SimpleBankAccount implements BankAccountWithAtm {

    private static final int FEE = 1;

    public SimpleBankAccountWithAtm(final AccountHolder holder, final double balance) {
        super(holder, balance);
    }

    @Override
    public final void depositWithAtm(final int usrID, final double amount) {
        super.deposit(usrID, amount - FEE);
    }

    @Override
    public final void withdrawWithAtm(final int usrID, final double amount) {
        super.withdraw(usrID, amount + FEE);
    }
}
